extends KinematicBody

#Copy pasted gravity vvvvvvv
var gravity = -9.8
var velocity = Vector3()

const SPEED = 2
const ACCELERATION = 3
const DE_ACCELERATION = 6
	#^^^^^^^^^^^^

# Hey don't flip gravity right away, wait
var waitflip = false

func _ready():
	pass

func _physics_process(delta):
	
	#Copy pasted gravity vvvvvvv
	var dir = Vector3()
	dir.y = 0
	dir = dir.normalized()
	velocity.y += delta * gravity
	var hv = velocity
	hv.y = 0
	var new_pos = dir * SPEED
	var accel = DE_ACCELERATION
	if (dir.dot(hv) > 0):
		accel = ACCELERATION
	hv = hv.linear_interpolate(new_pos, accel * delta)
	velocity.x = hv.x
	velocity.z = hv.z
	velocity = move_and_slide(velocity, Vector3(0,1,0))
	#^^^^^^^^^^^^
	
	
	if((translation.y < -10 || translation.y > 10) && !waitflip):
		flipGravity()
		
	if (translation.y < 5 && translation.y > -5):
		waitflip = false

# Flip the direction of gravity, set the wait flag to true so gravity
# doesn't immediately switch back
func flipGravity():
	print("Gravity flip")
	waitflip = true
	gravity = -gravity

# Collision trigger
func _on_Area_area_entered(area):
	print("Hit ground")
